'use strict';
module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-ts');

    // Show grunt task time
    require('time-grunt')(grunt);

    // Configurable paths for the app
    var appConfig = {
        admin_app: 'admin/app',
        admin_dist: 'admin/dist',
        admin_bowercomponents: 'admin/bower_components',
        admin_style_css: 'admin/app/styles/style.css',
        admin_style_less: 'admin/app/less/style.less',
        srv: 'dist/app.js'
    };

    // Grunt configuration
    grunt.initConfig({

        // Project settings
        config: appConfig,

        // The grunt server settings
        express: {
            options: {
                // Override defaults here 
            },
            dev: {
                options: {
                    script: '<%= config.srv %>'
                }
            },
            prod: {
                options: {
                    script: '<%= config.srv %>',
                    node_env: 'production',
                    background: false
                }
            }
        },

        ts: {
            default: {
                src: ['typings/index.d.ts', 'typings_manual/index.d.ts', 'src/**/*.ts'],
                outDir: 'dist',
                options: {
                    "target": "es6",
                    "module": "commonjs",
                    "sourceMap": true,
                    "emitDecoratorMetadata": true,
                    "experimentalDecorators": true,
                    "removeComments": false,
                    "noImplicitAny": false
                }
            }
        },

        // Compile less to css
        less: {
            development: {
                options: {
                    compress: true,
                    optimization: 2
                },
                files: {
                    "<%= config.admin_style_css %>": "<%= config.admin_style_less %>"
                }
            }
        },
        // Watch for changes in live edit
        watch: {
            styles: {
                files: ['admin/app/less/**/*.less'],
                tasks: ['less', 'copy:styles'],
                options: {
                    nospawn: true,
                    livereload: true
                },
            },
            js: {
                files: ['<%= config.admin_app %>/scripts/{,*/}*.js'],
                tasks: ['build-admin'],
                options: {
                    livereload: true
                }
            },
            livereload: {
                options: {
                    livereload: true
                },
                tasks: ['build-admin'],
                files: [
                    '<%= config.admin_app %>/**/*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= config.admin_app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            },
            server: {
                files: ['src/**/*'],
                tasks: ['build-server', 'express:dev'],
                options: {
                    spawn: false
                }
            }
        },
        // If you want to turn on uglify you will need write your angular code with string-injection based syntax
        // For example this is normal syntax: function exampleCtrl ($scope, $rootScope, $location, $http){}
        // And string-injection based syntax is: ['$scope', '$rootScope', '$location', '$http', function exampleCtrl ($scope, $rootScope, $location, $http){}]
        uglify: {
            options: {
                mangle: false
            }
        },
        // Clean dist folder
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= config.admin_dist %>/{,*/}*',
                        '!<%= config.admin_dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.admin_app %>',
                        dest: '<%= config.admin_dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            '*.html',
                            'views/{,*/}*.html',
                            'styles/patterns/*.*',
                            'img/{,*/}*.*',
                            'styles/fonts/*.*'
                        ]
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'admin/bower_components/fontawesome',
                        src: ['fonts/*.*'],
                        dest: '<%= config.admin_dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'admin/bower_components/bootstrap',
                        src: ['fonts/*.*'],
                        dest: '<%= config.admin_dist %>'
                    },
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= config.admin_app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },
        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= config.admin_dist %>/scripts/{,*/}*.js',
                    '<%= config.admin_dist %>/styles/{,*/}*.css',
                ]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.admin_dist %>',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: '<%= config.admin_dist %>'
                }]
            }
        },
        useminPrepare: {
            html: '<%= config.admin_app %>/index.html',
            options: {
                dest: 'admin/dist'
            }
        },
        usemin: {
            html: ['<%= config.admin_dist %>/index.html']
        }
    });

    // Build version for production
    grunt.registerTask('build-admin', [
        'clean:dist',
        'less',
        'useminPrepare',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('build-server', [
        'ts'
    ]);

    grunt.registerTask('default', [
        'build-admin',
        'build-server',
        'express:prod'
    ]);

    grunt.registerTask('dev', [
        'build-admin',
        'build-server',
        'express:dev',
        'watch'
    ]);

};
