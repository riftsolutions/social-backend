import express = require('express');
import multer = require('multer');
import path = require('path');
import fs = require('fs');
import gm = require('gm');
import rimraf = require('rimraf');
import async = require('async');
import mkdirp = require('mkdirp');
import ffmpeg = require('ffmpeg');

// Config
import {UploadSettings, AccountNames} from '../config';

// Post schema
import {PostDataType, Post, IPost} from '../schema/post';
// Launch schema
import {Launch, ILaunch} from '../schema/launch';
// Message schema
import {Message, IMessage} from '../schema/message';
// Contact schema
import {Contact, IContact} from '../schema/contact';

let uploadsPath = path.join(__dirname, '..', '..', UploadSettings.UploadsDir);

// Upload settings
var upload = multer({ dest: UploadSettings.UploadsDir + '/' + UploadSettings.TempDir });

var removeTempTimeouts = {};
var removeTempImagesForUUID = (uuid: string) => {
    rimraf(path.join(uploadsPath, UploadSettings.WaitingDir, uuid), (err) => {
        if (err) {
            console.error("Unable to remove temporary files for UUID: " + uuid);
        }
        console.log("Removed temporary files for UUID: " + uuid);
    });
}

var api = express.Router({mergeParams: true});

api.get('/account', (req, res) => {
    // account ID
    let account = req.session['account'];
    var accountID = account['account'];

    var accountName = AccountNames[accountID];
    if (!accountName) {
        accountName = "";
    }

    return res.json({
        account: accountID,
        sitename: accountName
    });
})

api.post('/upload/image', upload.single('file'), (req, res) => {

    if (!req.body || !req.body.uuid) {
        return res.status(400).send("UUID is required.");
    }

    let uuid = req.body.uuid;
    let image = req.file;

    let uuidWaitingFolder = path.join(uploadsPath, UploadSettings.WaitingDir, uuid);

    async.waterfall([
        (cb) => {
            mkdirp(uuidWaitingFolder, (err) => {
                if (err) {
                    console.error(err);
                }
                cb(err);
            });
        },
        (cb) => { // get all available image properties
            gm(image.path).identify((err) => {
                if (err) {
                    console.error(err);
                }
                cb(err);
            });
        },
        (cb) => { // Resize image and save in waiting folder
            var jpeg = path.join(uuidWaitingFolder, image.filename + '.jpg');
            gm(image.path).resize(1500).write(jpeg, (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log("Wrote converted image to: " + jpeg);
                }
                cb(err);
            });
        },
        (cb) => {
            // Image successfully resized and converted. Delete the temp one.
            fs.unlink(image.path);

            // Let's only keep these files waiting for a limited time.
            if (removeTempTimeouts[uuid]) {
                clearTimeout(removeTempTimeouts[uuid]);
            }
            removeTempTimeouts[uuid] = setTimeout(removeTempImagesForUUID, 10 * 60 * 1000, uuid); // in 10 minutes, we will remove the temp files for this uuid

            cb();
        }
    ], (err) => {
        if (err) {
            return res.status(400).send('Unable to upload image.');
        }
        res.send();
    });

});

api.post('/upload/video', upload.single('file'), (req, res) => {

    if (!req.body || !req.body.uuid) {
        return res.status(400).send("UUID is required.");
    }

    let uuid = req.body.uuid;
    let video = req.file;

    let uuidWaitingFolder = path.join(uploadsPath, UploadSettings.WaitingDir, uuid);

    async.waterfall([
        (cb) => {
            mkdirp(uuidWaitingFolder, (err) => {
                cb(err);
            });
        },
        (cb) => { // Move video to waiting folder
            var file = path.join(uuidWaitingFolder, video.filename + '.mp4');
            fs.rename(video.path, file, (err) => {
                if (!err) {
                    console.log("Moved uploaded video to: " + file);
                }
                cb(err);
            });
        },
        (cb) => {

            // Let's only keep these files waiting for a limited time.
            if (removeTempTimeouts[uuid]) {
                clearTimeout(removeTempTimeouts[uuid]);
            }
            removeTempTimeouts[uuid] = setTimeout(removeTempImagesForUUID, 10 * 60 * 1000, uuid); // in 10 minutes, we will remove the temp files for this uuid

            cb();
        }
    ], (err) => {
        if (err) {
            return res.status(400).send('Unable to upload video.');
        }
        res.send();
    });

});

api.post('/post', (req, res) => {
    let accountID = req.session['account']['account'];

    let dataType: PostDataType = req.body.dataType;

    if (!dataType || dataType < 1 || dataType >= PostDataType.Count) {
        return res.status(400).send("Invalid data type.");
    }

    let uploadRequired = (dataType == PostDataType.Photo) || (dataType == PostDataType.Video);

    // Check data.
    let title = req.body.title;
    let caption = req.body.caption;
    let cta = req.body.cta;

    var uuid: string;
    var waitingPath: string;
    if (uploadRequired) {
        uuid = req.body.uuid + '';
        waitingPath = path.join(uploadsPath, UploadSettings.WaitingDir, uuid);
    }

    async.waterfall([
        (cb) => { // Read the uuid's waiting directory.
            if (uploadRequired) {            
                fs.readdir(waitingPath, (err, files) => {
                    if (err) {
                        console.error(err);
                    }
                    cb(err, files);
                });
            } else {
                cb(null, []);
            }
        }, 
        (files: string[], cb) => { // For each media item in the directory, move it to the prod dir, save new path.
            var destination: string;
            switch (dataType) {
                case PostDataType.Photo: 
                    destination = UploadSettings.ImagesDir;
                    break;
                case PostDataType.Video:
                    destination = UploadSettings.VideosDir;
                    break;
                case PostDataType.Text:
                    return cb(null, {"text": req.body.data.text});
                case PostDataType.QA:
                    return cb(null, {"question": req.body.data.question, "answer": req.body.data.answer});
                default: return cb("Invalid post type");
            }

            async.map(files, (file, callback) => {
                var newFileName = destination + '/' + file;
                fs.rename(path.join(waitingPath, file), path.join(uploadsPath, newFileName), (err) => {
                    if (err) {
                        console.error(err);
                    }
                    getSizeOfFile(path.join(uploadsPath, newFileName), (err, size) => {
                        if (err) {
                            console.error(err);
                        }

                        var result = {};

                        result['url'] = path.join(UploadSettings.UploadsDir, newFileName); // uploads/images/... /uploads/videos/...
                        if (size && size.width) {
                            result['width'] = size.width;
                        }
                        if (size && size.height) {
                            result['height'] = size.height;
                        }
                        if (dataType == PostDataType.Video) {
                            result['loops'] = req.body.data.loops;
                            result['controls'] = req.body.data.controls;
                        }

                        callback(err, result);
                    });
                });
            }, (err, files) => {
                if (dataType == PostDataType.Photo) {
                    cb(err, {"images": files});
                } else if (dataType == PostDataType.Video) {
                    cb(err, files[0]);
                } else {
                    cb(err, files);
                }
                
            });
        },
        (data, cb) => { // Save the post now with image data.

            new Post({
                account: accountID,
                title: title,
                caption: caption,
                dataType: dataType,
                data: data,
                cta: cta
            }).save(cb);
        }
    ], (err) => {
        if (err) {
            return res.status(400).send('Unable to save post. ' + err.toString());
        }
        res.send();
    });
});

api.delete('/post/:id', (req, res) => {
    let id = req.params.id;

    let accountID = req.session['account']['account'];

    if (!id) {
        return res.status(400).send("No ID given");
    }

    Post.findOne({_id: id, account: accountID}).remove((err) => {
        if (err) {
            return res.status(400).send("Unable to delete post");
        }
        return res.send("Deleted post");
    });
});

api.get('/stats', (req, res) => {
	let ONE_DAY = 24 * 60 * 60 * 1000; /* ms */
    let ONE_WEEK = ONE_DAY * 7;
    let ONE_MONTH = ONE_DAY * 30;

    let accountID = req.session['account']['account'];

    Post.find({account: accountID}).exec((err, posts: IPost[]) => {
        Launch.find({account: accountID}).exec((err2, launches: ILaunch[]) => {
            Message.find({account: accountID, adminRead: false}).exec((err3, unread: IMessage[]) => {
                if (err || err2 || err3) {
                    return res.status(500).send('Unable to get stats.');
                }

                var views_week = 0;
                var views_month = 0;
                var likes_week = 0;
                var launches_week = 0;
                var launches_unique = 0;
                async.eachSeries(posts, (post, cb) => {
                    if ((new Date).getTime() - post.date < ONE_MONTH) {
                        views_month += post.views;
                    }
                    if ((new Date).getTime() - post.date < ONE_WEEK) {
                        views_week += post.views;
                        likes_week += post.likes;
                    }

                    cb();
                }, (err) => {
                    var launches_uuid = {};
                    async.eachSeries(launches, (launch, cb) => {

                        launches_week++;
                        if (!launches_uuid[launch.deviceUUID]) {
                            launches_unique++;
                            launches_uuid[launch.deviceUUID] = true;
                        }
                        
                        cb();
                    }, (err) => {
                        res.send({
                            "unread_messages": unread.length,
                            "views_week": views_week,
                            "views_month": views_month,
                            "likes_week": likes_week,
                            "launches_week": launches_week,
                            "launches_unique": launches_unique,
                        });
                    });
                });
            });
        });
    });
});

api.post('/message', (req, res) => {
    let accountID = req.session['account']['account'];
    let uuid = req.body.uuid;
    let text = req.body.text;
    if (!uuid || !text) {
        return res.status(400).send('Invalid parameters.');
    }

    new Message({
        account: accountID,
        user: uuid,
        fromAdmin: true,
        adminRead: true,
        text: text
    }).save((err) => {
        if (err) {
            return res.status(400).send('Unable to create message.');
        }
        res.send();
    });
});

api.get('/messages', (req, res) => {
    let accountID = req.session['account']['account'];

    Message.find({account: accountID}).sort('-date').exec((err, messages: IMessage[]) => {
        if (err) {
            return res.status(500).send('Unable to find messages.');
        }

        var conversations = {};
        var users = [];

        messages = messages.map((value: any, index, array) => {
            return value.toObject({versionKey: false});
        });

        messages.forEach(message => {
            let user = message.user;
            if (!conversations[user]) {
                conversations[user] = [];
                users.push(user);
            }
            conversations[user].push(message);
        });
        
        users = users.map((value, index, array) => {
            return conversations[value];
        })
        return res.json(users);
    });
});

api.post('/readMessages', (req, res) => {
    let uuid = req.body.uuid;
    let accountID = req.session['account']['account'];

    if (!uuid) {
        return res.status(400).send('Invalid parameters');
    }

    Message.update({account: accountID, user: uuid}, { $set: {adminRead: true} }, {multi: true, safe: false}).exec((err) => {
        if (err) {
            return res.status(400).send('Unable to update messages');
        }
        res.send();
    });
});


api.get('/contacts', (req, res) => {
    let accountID = req.session['account']['account'];

    Contact.find({account: accountID}).exec((err, contacts: IContact[]) => {
        if (err) {
            return res.status(500).send('Unable to get contacts.');
        }

        return res.json(contacts || []);
    });
})


function getSizeOfFile(path: string, callback) {
    if (path.indexOf('.jpg') !== -1) {
        gm(path).size(callback);
    } else if (path.indexOf('.mp4') !== -1) {
        var process = new ffmpeg(path);
        process.then(function (video) {
            callback(null, {
                'width': video.metadata.video.resolution.w,
                'height': video.metadata.video.resolution.h
            });
        }, function (err) {
            callback(err);
        });
    } else {
        callback(true);
    }
    
}

export const AdminApiRoutes = api;
