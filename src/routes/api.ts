import express = require('express');
import multer = require('multer');
import path = require('path');
import async = require('async');

// Post schema
import {PostDataType, Post, IPost} from '../schema/post';
// Launch schema
import {Launch, ILaunch} from '../schema/launch';
// Message schema
import {Message, IMessage} from '../schema/message';
// Contact schema
import {Contact, IContact} from '../schema/contact';

var api = express.Router({mergeParams: true});

/*
 * Retrieves a list of posts, sorted by date created.
 */
api.get('/posts', (req, res) => {
    Post.find({account: req.params.account}).sort('-date').exec((err, posts: IPost[]) => {
        if (err) {
            return res.status(500).send('Unable to find posts.');
        }
        posts = posts.map((value: any, index, array) => {
            return value.toObject({versionKey: false});
        });
        
        res.json(posts);
    });
});

/*
 * Performs a like action on the post. Simply increments the like count.
 * // TODO: limit this to one like per user.
 */
api.post('/like', (req, res) => {
    let postID = req.body.post;
    if (!postID) {
        return res.status(400).send('No postID given.');
    }

    Post.findOneAndUpdate({_id: postID, account: req.params.account}, {$inc: {likes: 1}}, (err, post: IPost) => {
        if (err || !post) {
            return res.status(400).send('Unable to save like.');
        }
        res.send();
    });
});

/*
 * Performs a like action on the post. Simply increments the like count.
 * // TODO: limit this to one like per user.
 */
api.post('/unlike', (req, res) => {
    let postID = req.body.post;
    if (!postID) {
        return res.status(400).send('No postID given.');
    }

    Post.findOneAndUpdate({_id: postID, account: req.params.account}, {$inc: {likes: -1}}, (err, post: IPost) => {
        if (err || !post) {
            return res.status(400).send('Unable to save like.');
        }
        res.send();
    });
});

/*
 * Performs a view action on the post. Simply increments the views count.
 * // TODO: limit this to one view per user.
 */
api.post('/views', (req, res) => {
    let postID = req.body.post;
    if (!postID) {
        return res.status(400).send('No postID given.');
    }

    Post.findOneAndUpdate({_id: postID, account: req.params.account}, {$inc: {views: 1}}, (err, post: IPost) => {
        if (err || !post) {
            return res.status(400).send('Unable to save view.');
        }
        res.send();
    });
});

/*
 * Sends a message from the given UUID to the admins.
 */
api.post('/message', (req, res) => {
    let uuid = req.body.uuid;
    let text = req.body.text;
    if (!uuid || !text) {
        return res.status(400).send('Invalid parameters.');
    }

    new Message({
        account: req.params.account,
        user: uuid,
        fromAdmin: false,
        text: text
    }).save((err) => {
        if (err) {
            return res.status(400).send('Unable to create message.');
        }
        res.send();
    });
});

/*
 * Gets messages from the UUID given
 */
api.get('/messages', (req, res) => {
	let uuid = req.query.uuid;
	if (!uuid) {
		return res.status(400).send('No UUID given.');
	}

    Message.find({account: req.params.account, user: uuid}).sort('date').exec((err, messages: IMessage[]) => {
        if (err) {
            return res.status(500).send('Unable to find messages.');
        }
        messages = messages.map((value: any, index, array) => {
            return value.toObject({versionKey: false});
        });
        
        res.json(messages);
    });
});

api.post('/registerLaunch', (req, res) => {
    let uuid = req.body.uuid;
    if (!uuid) {
        return res.status(400).send('No UUID given.');
    }

    new Launch({
        account: req.params.account,
        deviceUUID: uuid
    }).save((err) => {
        res.send();
    });
});

api.post('/userInfo', (req, res) => {
    let uuid = req.body.uuid;
    let email = req.body.email;
    let name = req.body.name;
    if (!uuid || !email || !name) {
        return res.status(400).send('Invalid parameters.');
    }

    new Contact({
        account: req.params.account,
        deviceUUID: uuid,
        email: email,
        name: name
    }).save((err) => {
        res.send();
    });
});

export const ApiRoutes = api;
