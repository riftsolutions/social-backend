import express = require('express');

// Post schema
import {PostDataType, Post} from '../schema/post';

var api = express.Router({mergeParams: true});

api.get('/', (req, res) => {
    let postID = req.params.postID;
    Post.findOne({_id: postID}).exec((err, post) => {
        if (!err && post) {
            res.render('share', {post: post, title: post.title});
        } else {
            res.status(404).render('404');
        }
    });
});

export const ShareRoute = api;
