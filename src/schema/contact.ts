import mongoose = require('mongoose');

import {Account} from '../config';

export const ContactSchemaName = 'Contact';

// Typescript interface
export interface IContact extends mongoose.Document {
    account: Account,
    deviceUUID: string,
    email: string;
    name: string;
};

// Mongoose schema
export const ContactSchema = new mongoose.Schema({
    account: Number,
    deviceUUID: String,
    email: String,
    name: String
});

// Create mongoose model
export const Contact = mongoose.model<IContact>(ContactSchemaName, ContactSchema);
