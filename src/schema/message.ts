import mongoose = require('mongoose');

import {Account} from '../config';

export const MessageSchemaName = 'Message';

// Typescript interface
export interface IMessage extends mongoose.Document {
    account: Account,
    user: string;
    fromAdmin: boolean;
    text: string;
    date: number;
    adminRead: boolean;
};

// Mongoose schema
export const MessageSchema = new mongoose.Schema({
    account: Number,
    user: String,
    fromAdmin: Boolean,
    text: {type: String, default: ""},
    date: {type: Date, default: Date.now},
    adminRead: {type: Boolean, default: false}
});

// Create mongoose model
export const Message = mongoose.model<IMessage>(MessageSchemaName, MessageSchema);
