import mongoose = require('mongoose');

import {Account} from '../config';

export const LaunchSchemaName = 'Launch';

// Typescript interface
export interface ILaunch extends mongoose.Document {
    account: Account,
    deviceUUID: string,
    date: number;
};

// Mongoose schema
export const LaunchSchema = new mongoose.Schema({
    account: Number,
    deviceUUID: String,
    date: {type: Date, default: Date.now}
});

// Create mongoose model
export const Launch = mongoose.model<ILaunch>(LaunchSchemaName, LaunchSchema);
