import mongoose = require('mongoose');

import {Account} from '../config';

export const PostSchemaName = 'Post';

// Available post types
export enum PostDataType {
    Photo = 1,
    Video = 2,
    Text = 3,
    QA = 4,

    Count
}

export interface IPostCTA {
    url: String;
    title: String
}

// Typescript interface
export interface IPost extends mongoose.Document {
    account: Account,
    title: string;
    caption: string;
    date: number;
    likes: number;
    views: number;
    dataType: PostDataType;
    data: any;
    cta: IPostCTA;
};

// Mongoose schema
export const PostSchema = new mongoose.Schema({
    account: Number,
    title: String,
    caption: {type: String, default: ""},
    date: {type: Date, default: Date.now},
    likes: {type: Number, default: 0},
    views: {type: Number, default: 0},
    dataType: Number,
    data: Object,
    cta: Object
});

// Create mongoose model
export const Post = mongoose.model<IPost>(PostSchemaName, PostSchema);
