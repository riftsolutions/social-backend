

export enum Account {
    SeanDougherty = 1,

    Count
};

export const AccountNames = {
    1: "seandshoots"
}

export const Users = {
    "sean": {
        "name": "Sean Dougherty",
        "password": "seandshoots!",
        "account": 1
    }
}

export const UploadSettings = {
    UploadsDir: 'uploads',
    TempDir: 'tmp', // Temporary directory for files being uploaded / converted.
    WaitingDir: 'waiting', // Uploaded but not attached to a post. Will be cleaned periodically.
    ImagesDir: 'images', // Public images attached to posts
    VideosDir: 'videos' // Public videos attached to posts
}

export const DatabaseSettings = {
    Host: 'db01.ianmcdowell.net',
    Port: '27017',
    Database: 'social',
    User: 'social',
    Password: 'PO0KeqDVkfNe2d0n',
    SessionSecret: 'As3elt83djbhljaer90'
}
