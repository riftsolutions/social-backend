import express = require('express');
import mongoose = require('mongoose');
import bodyParser = require('body-parser');
import logger = require('morgan');
import rimraf = require('rimraf');
import path = require('path');
import exphbs = require('express-handlebars');
import session = require('express-session');
import connectMongo = require('connect-mongo');

// Schemas
import {Post} from './schema/post';

// Routes
import {ApiRoutes} from './routes/api';
import {AdminApiRoutes} from './routes/admin';
import {ShareRoute} from './routes/share';

// Config
import {DatabaseSettings, Account, UploadSettings, Users} from './config';

var app = express();
var MongoStore = connectMongo(session);

// Database connection
mongoose.connect(DatabaseSettings.User + ':' + DatabaseSettings.Password + '@' + DatabaseSettings.Host + ':' + DatabaseSettings.Port + '/' + DatabaseSettings.Database);

app.set('views', path.join(__dirname, '../share'));
app.engine('handlebars', exphbs({
    defaultLayout: 'main',
    layoutsDir:'share/layouts',
    partialsDir:'share/partials',
    helpers: {
        'ifCond': function(v1, v2, options) {
            if(v1 === v2) {
                return options.fn(this);
            }
            return options.inverse(this);
        }
    }
}));
app.set('view engine', 'handlebars');
app.use(logger('dev'));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
app.use(session({
    secret: DatabaseSettings.SessionSecret,
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));


// Setup routes
var checkAccount = (req, res, next) => {
    var account = req.params.account;
    if (!account || account < 1 || account >= Account.Count) {
        return res.status(400).send('Invalid account.');
    }
    next();
}

var checkLogin = (req, res, next) => {
    if (!req.session || !req.session['account']) {
        return res.redirect('/');
    }
    next();
}

app.get('/', (req, res, next) => {
    if (req.session && req.session['account']) {
        return res.redirect('/admin');
    }
    next();
})
app.use('/', express.static('static'));

app.post('/login', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    if (!username || !password || username.length == 0 || password.length == 0) {
        return res.redirect('/');
    }

    // check if creds are valid
    let account = Users[username];
    if (!account) {
        return res.redirect('/');
    }

    if (account.password !== password) {
        return res.redirect('/');
    }

    // valid at this point.
    req.session["account"] = account;

    return res.redirect('/admin');
});

app.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        res.redirect('/');
    });
});

app.use('/api/:account/', checkAccount, ApiRoutes);

app.use('/share/:postID', ShareRoute);


app.use('/admin', checkLogin, express.static('admin/dist'));
app.use('/admin/api', checkLogin, AdminApiRoutes);

// Image uploads
app.use('/uploads/', express.static('uploads'));

// iOS 9 deep links
app.get('/apple-app-site-association', (req, res) => {
    return res.json({
        "applinks": {
            "apps": [],
            "details": [
                {
                    "appID": "7RM2H458UA.solutions.rift.seandshoots-ios",
                    "paths":[ "*" ]
                }
            ]
        }
    });
});

// Clear old waiting dir and temp dir
rimraf(path.join(__dirname, UploadSettings.UploadsDir, UploadSettings.WaitingDir, '*'), (err) => {
    if (err) {console.error("Unable to remove waiting files");}
    console.log("Removed waiting files");
});
rimraf(path.join(__dirname, UploadSettings.UploadsDir, UploadSettings.TempDir, '*'), (err) => {
    if (err) {console.error("Unable to remove temp files");}
    console.log("Removed temp files");
});

var port: number = +process.env.PORT || 3000;

var server = app.listen(port, function() {
    console.log('Express server listening on port: ' + port);
});
