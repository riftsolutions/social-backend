# README #

### What is this repository for? ###

The backend for the social app is strictly divided into two parts, the API and the admin portal. The admin portal resides in the `admin/` directory, and the API resides in the `src/` directory. Both are built using `grunt`.

This project relies on a MongoDB database, who's connection settings are set in `src/config.ts`

### How do I get set up? ###

Ensure node is installed and at the latest version.
The following global npm packages are required: `bower`, `grunt-cli`, `typings`.
The following extra packages are required: `imagemagick`, `graphicsmagick`

* `npm install`
* `bower install`
* `typings install`

Set configuration in `src/config.ts`

* `grunt`

### Who do I talk to? ###

* Repo owner: Ian McDowell
