
/**
 * MainCtrl - controller
 */
function MainCtrl($scope, $http) {

    $scope.account = 0;
    $scope.sitename = "";

    var getAccount = function() {
        $http.get('/admin/api/account')
        .success(function(data) {
            $scope.account = data['account'];
            $scope.sitename = data['sitename'];
        })
        .error(function(data,status,error,config){
            alert("Unable to load account.");
        });
    }
    getAccount();

};

function HomeCtrl($scope, $http) {

    $scope.unread_messages = 0;
    $scope.views_week = 0;
    $scope.views_month = 0;
    $scope.likes_week = 0;
    $scope.launches_week = 0;
    $scope.launches_unique = 0;

    var getStats = function() {
        $http.get('/admin/api/stats')
        .success(function(data) {
            $scope.unread_messages = data['unread_messages'];
            $scope.views_week = data['views_week'];
            $scope.views_month = data['views_month'];
            $scope.likes_week = data['likes_week'];
            $scope.launches_week = data['launches_week'];
            $scope.launches_unique = data['launches_unique'];
        })
        .error(function(data,status,error,config){
            alert("Unable to load stats.");
        });
    };
    getStats();
};

function PostsCtrl($scope, $http) {

    $scope.posts = [];
    
    var getPosts = function() {
        $http.get('/api/' + $scope.account + '/posts')
        .success(function(data) {
            $scope.posts = data;
        })
        .error(function(data,status,error,config){
            $scope.posts = null;
            alert("Unable to load posts.");
        });
    };
    getPosts();

    $scope.deletePost = function(post) {
        if (!confirm("Are you sure you want to delete this post?")) return;
        console.log("Deleting post: " + JSON.stringify(post));

        $http.delete('/admin/api/post/' + post._id)
        .success(function(data) {
            getPosts();
        })
        .error(function(data, status, error, config) {
            alert("Unable to delete post.");
        });
    };
};

function EditPostCtrl($scope, $state, $stateParams, $location) {

    if (!$stateParams.post) {
        $location.path("/");
        return;
    }

    $scope.post = $stateParams.post;

    $scope.submit = function() {
        
    };

};

function NewImagePostCtrl($scope, $http, $state, $location) {

    var genUUID = function() {
        $scope.postUUID = Math.floor(1000000000 + Math.random() * 9000000000);
    }

    $scope.postUUID = null;
    $scope.title = "";
    $scope.caption = "";
    $scope.cta_title = "";
    $scope.cta_url = "";

    $scope.uploaded = false;

    genUUID();

    $scope.dropzone = function() {
        new Dropzone("div#dropzone-image", { url: '/admin/api/upload/image', acceptedFiles: 'image/*', params: { 'uuid': $scope.postUUID }, queuecomplete: function() {
            $scope.uploaded = true;
        } });
    }

    $scope.submit = function() {
        if (!$scope.uploaded) {
            return alert("Please upload at least one image.");
        }

        var post = {
            uuid: $scope.postUUID,
            dataType: 1, // image
            title: $scope.title,
            caption: $scope.caption
        };

        if ($scope.cta_title && $scope.cta_url) {
            post.cta = {
                title: $scope.cta_title,
                url: $scope.cta_url
            };
        }

        $http.post('/admin/api/post', post)
        .success(function(data) {
            alert("Posted successfully.");
            $location.path("/posts");
        })
        .error(function(data,status,error,config){
            alert("Unable to post. Try again later.");
        });
    }
}

function NewVideoPostCtrl($scope, $http, $state, $location) {

    var genUUID = function() {
        $scope.postUUID = Math.floor(1000000000 + Math.random() * 9000000000);
    }

    $scope.postUUID = null;
    $scope.title = "";
    $scope.caption = "";
    $scope.loops = false;
    $scope.controls = true;
    $scope.cta_title = "";
    $scope.cta_url = "";

    $scope.uploaded = false;

    genUUID();

    $scope.dropzone = function() {
        new Dropzone("div#dropzone-video", { url: '/admin/api/upload/video', maxFiles: 1, acceptedFiles: 'video/mp4', params: { 'uuid': $scope.postUUID }, queuecomplete: function() {
            $scope.uploaded = true;
        } });
    }

    $scope.submit = function() {
        if (!$scope.uploaded) {
            return alert("Please upload a video first.");
        }

        var post = {
            uuid: $scope.postUUID,
            dataType: 2, // video
            title: $scope.title,
            caption: $scope.caption,
            data: {
                loops: $scope.loops,
                controls: $scope.controls
            }
        };

        if ($scope.cta_title && $scope.cta_url) {
            post.cta = {
                title: $scope.cta_title,
                url: $scope.cta_url
            };
        }

        $http.post('/admin/api/post', post)
        .success(function(data) {
            alert("Posted successfully.");
            $location.path("/posts");
        })
        .error(function(data,status,error,config){
            alert("Unable to post. Try again later.");
        });
    }
}

function NewTextPostCtrl($scope, $http, $state, $location) {

    $scope.text = "";
    $scope.cta_title = "";
    $scope.cta_url = "";

    $scope.submit = function() {
        var post = {
            dataType: 3, // text
            data: {
                text: $scope.text,
            }
        };

        if ($scope.cta_title && $scope.cta_url) {
            post.cta = {
                title: $scope.cta_title,
                url: $scope.cta_url
            };
        }

        $http.post('/admin/api/post', post)
        .success(function(data) {
            alert("Posted successfully.");
            $location.path("/posts");
        })
        .error(function(data,status,error,config){
            alert("Unable to post. Try again later.");
        });
    }
}

function NewQAPostCtrl($scope, $http, $state, $location) {

    $scope.question = "";
    $scope.answer = "";

    $scope.submit = function() {
        var post = {
            dataType: 4, // QA
            data: {
                question: $scope.question,
                answer: $scope.answer
            }
        };

        $http.post('/admin/api/post', post)
        .success(function(data) {
            alert("Posted successfully.");
            $location.path("/posts");
        })
        .error(function(data,status,error,config){
            alert("Unable to post. Try again later.");
        });
    }
}

function MessagesCtrl($scope, $http) {
    $scope.messages = [];
    
    var getMessages = function() {
        $http.get('/admin/api/messages')
        .success(function(data) {
            $scope.messages = data;
        })
        .error(function(data,status,error,config){
            $scope.messages = null;
            alert("Unable to load messages.");
        });
    };
    getMessages();
}

function ConversationCtrl($scope, $state, $stateParams, $location, $http) {

    if (!$stateParams.messages) {
        $location.path("/messages");
        return;
    }

    $scope.messages = $stateParams.messages.reverse(); // oldest to newest
    $scope.replyText = ""

    var markAsRead = function() {
        $http.post('/admin/api/readMessages', {uuid: $scope.messages[0].user})
        .success(function() {
            console.log("Marked as read.");
        })
        .error(function() {
            console.error("Unable to mark messages as read.");
        });
    };
    markAsRead();

    $scope.submit = function() {
        $http.post('/admin/api/message', {
            uuid: $scope.messages[0].user,
            text: $scope.replyText
        })
        .success(function(data) {
            $scope.messages.push({
                text: $scope.replyText,
                fromAdmin: true,
                date: Date.now()
            });
            $scope.replyText = "";
        })
        .error(function(data,status,error,config){
            alert("Unable to post. Try again later.");
        });
    };

};

angular
    .module('social')
    .controller('MainCtrl', MainCtrl)
    .controller('HomeCtrl', HomeCtrl)
    .controller('PostsCtrl', PostsCtrl)
    .controller('EditPostCtrl', EditPostCtrl)
    .controller('NewImagePostCtrl', NewImagePostCtrl)
    .controller('NewVideoPostCtrl', NewVideoPostCtrl)
    .controller('NewTextPostCtrl', NewTextPostCtrl)
    .controller('NewQAPostCtrl', NewQAPostCtrl)
    .controller('MessagesCtrl', MessagesCtrl)
    .controller('ConversationCtrl', ConversationCtrl)
