
(function () {
    angular.module('social', [
        'ui.router',                    // Routing
        'ui.bootstrap',                 // Bootstrap
        'slick'
    ])
})();
