
function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/index");

    $stateProvider

        .state('index', {
            url: "/index",
            templateUrl: "views/home.html",
            data: { pageTitle: 'Home' }
        })
        .state('posts', {
            url: "/posts",
            templateUrl: "views/posts.html",
            data: { pageTitle: 'Posts' }
        })
        .state('new-post', {
            url: "/new-post",
            templateUrl: "views/new-post.html",
            data: { pageTitle: 'New Post' }
        })
        .state('edit-post', {
            url: "/edit-post",
            templateUrl: "views/edit-post.html",
            data: { pageTitle: 'Edit Post' },
            params: { post: null }
        })
        .state('messages', {
            url: "/messages",
            templateUrl: "views/messages.html",
            data: { pageTitle: 'Messages' }
        })
        .state('conversation', {
            url: "/conversation",
            templateUrl: "views/conversation.html",
            data: { pageTitle: 'Conversation' },
            params: { messages: null }
        })
}
angular
    .module('social')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
